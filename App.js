import React, { useState } from 'react';
import { StyleSheet, View, Button } from 'react-native';

import AdiraModal from './components/AdiraModal';

export default function App() {
  const [ isVisible, setVisible ] = useState(false);

  const closeModal = () => {
    setVisible(false);
  };

  return ( 
    <View style={styles.screen}>
      <Button title="Show Modal" onPress={() => setVisible(true)} />
      <AdiraModal 
        isVisible={isVisible}
        closeModal={closeModal}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  screen: {
    padding: 50
  },
});
