import React from 'react';
import { StyleSheet, View, Modal, Text, Image, TouchableHighlight, Dimensions, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';

const AdiraModal = ({ isVisible, closeModal }) => {
  return (
    <Modal 
      visible={isVisible}
      transparent={true}
    >
      <View style={styles.container}>
        <View style={styles.box}>
          <View style={styles.imageContainer}>
            <Image 
              source={require('../assets/images/adira.jpeg')}
              style={styles.adiraImg}
            />
            <Icon name="closecircleo" style={styles.closeImg} size={17} onPress={closeModal} />
          </View>
          <Text style={styles.text}>Selamat! Dengan mengaktivasi akun adiraku, kamu sekaligus terdaftar di adira poin</Text>
          <TouchableHighlight
            onPress={closeModal}
            style={styles.button}
          >
            <Text style={styles.buttonText}>Lihat Poin Saya</Text>
          </TouchableHighlight>
        </View>
          <TouchableOpacity 
            onPress={closeModal}
            style={styles.closeContainer}
          >
            <Icon name="closecircleo" style={styles.closeImg}/>
          </TouchableOpacity>
      </View>
    </Modal>
  )
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(80,80,80,0.3)'
  },
  box: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    width: 250,
  },
  imageContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  textContainer: {
    backgroundColor: '#fff'
  },
  text: {
    padding: 20,
    paddingTop: 60,
    textAlign: 'center',
    backgroundColor: '#fff',
    borderTopLeftRadius: 7,
    borderTopRightRadius: 7,
  },
  button: {
    width: 250,
    backgroundColor: '#E74C3C',
    padding: 10,
    borderBottomLeftRadius: 7,
    borderBottomRightRadius: 7
  },
  buttonText: {
    color: '#fff',
    fontWeight: 'bold',
    textAlign: 'center'
  },
  adiraImg: {
    width: 100,
    height: 100,
    borderRadius: 100 / 2,
    position: 'absolute',
    alignItems: 'center',
    top: -20,
    left: (Dimensions.get('window').width / 2 - 212.5),
    zIndex: 1
  },
  closeContainer: {
    width: 15,
    height: 15,
    margin: 10,
    zIndex: 1,
    left: 110,
    top: 35
  },
  closeImg: {
    width: 20,
    height: 20,
    margin: 10,
    zIndex: 2,
    left: 110,
    top: 35,
  },
});

export default AdiraModal;